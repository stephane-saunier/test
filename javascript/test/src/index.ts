import cors from 'cors';
import express from 'express';
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

app.get('/', (req: any, res: { send: (arg0: string) => void; }) => {
    res.send('Hello World, from express');
})

app.use(cors());
app.use(bodyParser.json());
app.listen(port, () => console.log(`Hello world app listening on port ${port}!`))
